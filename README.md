# AdressDataGouvFr 8.x-1.x

# Feature 
This module add a new provider to Geocoder.

This provider use the [adresse.data.gouv.fr API](https://adresse.data.gouv.fr/api) for geocoding (and reverse) locations in France (only, sorry)

# Limitations
Actually, no usage restriction are coded in the module.
please check [API restriction](https://adresse.data.gouv.fr/faq)

For non french-speaker, api limitations are : 
- 10 requests per second and IP.
- 1 request simultaneous per IP.